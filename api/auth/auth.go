package auth

import (
	// buitin
	"fmt"

	// vendored
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
)

func CreateToken(uname string, kid string, action string) (string, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	
	if action == "AUTH" {
		claims["username"] = uname
	} else if action == "API" {
		claims["id"] = kid
	}
	
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	
	if tokenStr, err := token.SignedString([]byte(viper.GetString("api.secret_key"))); err != nil {
		return "", err
	} else {
		return tokenStr, nil
	}
}

func ValidateToken(tokenString string) (string, string, error) {	
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(viper.GetString("api.secret_key")), nil
	})
	if err != nil {
		return "", "", err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if uname, found := claims["username"].(string); found {
			return string(uname), "", nil
		}
		
		if kid, found := claims["id"].(string); found {
			return "", string(kid), nil
		}
		
		return "", "", fmt.Errorf("malformed authtoken")
	}
	return "", "", fmt.Errorf("invalid authtoken")
}