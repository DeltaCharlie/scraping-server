package models

import (
	// builtin
	"context"
	"fmt"
	"html"
	"runtime/debug"
	"time"
	"strings"
	
	// self
	"gitlab.com/DeltaCharlie/scraping-server/api/auth"
	"gitlab.com/DeltaCharlie/scraping-server/api/security"
	"gitlab.com/DeltaCharlie/scraping-server/api/db"
	
	// vendored
	"github.com/apex/log"
	"github.com/globalsign/mgo/bson"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type APIKey struct {
	ID          bson.ObjectId `json:"_id" bson:"_id"`
	Key         string        `json:"key" bson:"key"`
	Status      string        `json:"status" bson:"status"`
	CreateTs time.Time     `json:"create_ts" bson:"create_ts"`
}

func (ak *APIKey) Create() error {
	var err error
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	if err := db.DBClient.Connect(ctx); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return err
	}
	
	defer db.DBClient.Disconnect(ctx)
	
	col := db.DBClient.Database("scphero").Collection("apikeys")
	ak.ID = bson.NewObjectId()
	
	if ak.Key, err = auth.CreateToken("ANY", ak.ID.Hex(), "API"); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return err
	}
	
	ak.Key = security.Hash(ak.Key)
	
	ak.Key = html.EscapeString(strings.TrimSpace(ak.Key))
	ak.Status = html.EscapeString(strings.TrimSpace(ak.Status))
	ak.CreateTs = time.Now()
	
	if res, eRr := col.InsertOne(ctx, ak); err != nil {
		debug.PrintStack()
		log.Fatal(eRr.Error())
		return err
	} else {
		fmt.Println("Created APIKey: ", res.InsertedID)
	}

	return nil
}

func GetKeys(filter interface{}) ([]APIKey, error) {
	var keys []APIKey
	
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	if err := db.DBClient.Connect(ctx); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return keys, err
	}
	
	defer db.DBClient.Disconnect(ctx)
	
	col := db.DBClient.Database("scphero").Collection("apikeys")
	opts := options.Find()
	opts.SetSort(bson.D{{"create_ts", -1}})
	
	if csr, err := col.Find(ctx, filter, opts); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return keys, err
	} else {
		defer csr.Close(ctx)
		if err := csr.All(ctx, &keys); err != nil {
			debug.PrintStack()
			log.Fatal(err.Error())
			return keys, err
		}
	}

	return keys, nil
}

func GetKeysCount(filter interface{}) (int64, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	if err := db.DBClient.Connect(ctx); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return 0, err
	}
	
	defer db.DBClient.Disconnect(ctx)
	
	col := db.DBClient.Database("scphero").Collection("apikeys")

	if count, err := col.CountDocuments(ctx, filter); err != nil {
		return 0, err
	} else {
		return count, nil
	}

	
}

func CheckAPIKey(token string) error {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	if err := db.DBClient.Connect(ctx); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return err
	}
	
	defer db.DBClient.Disconnect(ctx)
	
	col := db.DBClient.Database("scphero").Collection("apikeys")

	if count, err := col.CountDocuments(ctx, bson.D{{"key", token}}); err != nil {
		return errors.New("unauthorized")
	} else {
		fmt.Println("Found matching keys:", count)
		return nil
	}	
}

func UpdateKeyStatus(kid bson.ObjectId, status string) error {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	if err := db.DBClient.Connect(ctx); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return err
	}
	
	defer db.DBClient.Disconnect(ctx)
	
	col := db.DBClient.Database("scphero").Collection("apikeys")
	status = html.EscapeString(strings.TrimSpace(status))
	update := bson.D{{"$set", bson.D{{"status", status}}}}
	
	if res, err := col.UpdateOne(ctx, bson.M{"_id": kid}, update); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return err
	} else {
		fmt.Printf("Updated %v APIKey!\n", res.ModifiedCount)
	}
	
	return nil
}

func RemoveKey(kid bson.ObjectId) error {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	if err := db.DBClient.Connect(ctx); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return err
	}
	
	defer db.DBClient.Disconnect(ctx)
	
	col := db.DBClient.Database("scphero").Collection("apikeys")

	if res, err := col.DeleteOne(ctx, bson.M{"_id": kid}); err != nil {
		debug.PrintStack()
		log.Fatal(err.Error())
		return err
	} else {
		fmt.Printf("Deleted %v APIKey!\n", res.DeletedCount)
	}
	
	return nil
}