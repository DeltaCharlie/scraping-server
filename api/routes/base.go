package routes

import (
	// builtin
	"net/http"

	// vendored
	"github.com/gin-gonic/gin"
)

type Response struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Error   string      `json:"error"`
}

type ListResponse struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Total   int64         `json:"total"`
	Data    interface{} `json:"data"`
	Error   string      `json:"error"`
}

func Ping(c *gin.Context) {
	c.JSON(http.StatusOK, Response{
		Status:  "ok",
		Message: "success",
		Data:    "The API is up and running",
	})
}