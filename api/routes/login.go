package routes

import (
	// builtin
	"net/http"

	// self
	"gitlab.com/DeltaCharlie/scraping-server/api/auth"
	"gitlab.com/DeltaCharlie/scraping-server/api/models"
	
	// vendored
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type UserData struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func Login(c *gin.Context) {

	var reqData UserData
	if err := c.ShouldBindJSON(&reqData); err != nil {
		HandleError(http.StatusUnauthorized, c, errors.New("not authorized"))
		return
	}

	user := models.User{
		Username: viper.GetString("api.username"),
		Password: viper.GetString("api.password"),
		Email: viper.GetString("api.email"),
	}
	if reqData.Username != user.Username || reqData.Password != user.Password {
		HandleError(http.StatusUnauthorized, c, errors.New("not authorized"))
		return
	}

	if tokenStr, err := auth.CreateToken(user.Username, "", "AUTH"); err != nil {
		HandleError(http.StatusUnauthorized, c, errors.New("not authorized"))
		return
	} else {
		c.JSON(http.StatusOK, Response{
			Status:  "ok",
			Message: "success",
			Data:    tokenStr,
		})
	}
}