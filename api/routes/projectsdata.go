package routes

import (
	// builtin
	"net/http"

	// vendored
	"github.com/gin-gonic/gin"
)

type TaskListRequestData struct {
	PageNum    int    `form:"page_num"`
	PageSize   int    `form:"page_size"`
	NodeId     string `form:"node_id"`
	SpiderId   string `form:"spider_id"`
	ScheduleId string `form:"schedule_id"`
	Database     string `form:"status"`
}

func GetProjectData(c *gin.Context) {
	c.JSON(http.StatusOK, Response{
		Status:  "ok",
		Message: "success",
		Data:    "Currently Not Serving any data",
	})
}